package com.talixa.eca.listeners;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class DefaultWindowListener implements WindowListener {

	// Default everything to do nothing so a subclass can just implement desired functionality
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		// DO NOTHING
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// DO NOTHING
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// DO NOTHING
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// DO NOTHING
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// DO NOTHING
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// DO NOTHING
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// DO NOTHING
	}
}
