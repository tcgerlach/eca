package com.talixa.eca.listeners;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class ExitActionListener implements ActionListener {
	
	private Window window;
	
	public ExitActionListener(Window w) {
		this.window = w;
	}
	
	public void actionPerformed(ActionEvent e) {
		WindowListener[] listeners = window.getWindowListeners();
		for(WindowListener listener : listeners) {
			listener.windowClosing(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
		}
		window.dispose();		
	}	
}
