package com.talixa.eca;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import com.talixa.eca.frames.FrameAbout;
import com.talixa.eca.listeners.DefaultWindowListener;
import com.talixa.eca.listeners.ExitActionListener;
import com.talixa.eca.shared.ECAConstants;
import com.talixa.eca.shared.IconHelper;
import com.talixa.eca.widgets.ECAPanel;

public class ECA {

	private static JFrame frame;
	private static ECAPanel eca;
	
	private static void createAndShowGUI() {
		frame = new JFrame(ECAConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.addWindowListener(new DefaultWindowListener());
		
		eca = new ECAPanel();
		frame.add(eca);
		
		// set icon
		IconHelper.setIcon(frame);	
													
		// add menus and toolbar
		addMenus();			
		addToolbar();
		
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(ECAConstants.APP_WIDTH,ECAConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (ECAConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (ECAConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
	}
	
	private static int currentRule = 30;
	
	private static void addToolbar() {
		JToolBar toolbar = new JToolBar();
		toolbar.setLayout(new FlowLayout(FlowLayout.CENTER));
		toolbar.setFloatable(false);
		
		// Using images from: http://www.oracle.com/technetwork/java/index-138612.html
		ClassLoader cl = ECA.class.getClassLoader();
		
		// left/right buttons with a text field in the middle
		final JLabel rule = new JLabel("Rule: " + currentRule);
		
		final JButton left = new JButton(new ImageIcon(cl.getResource("res/left.gif")));
		left.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				currentRule--;
				eca.showRule(currentRule);
				rule.setText("Rule: " + currentRule);
			}
		});
		
		final JButton right = new JButton(new ImageIcon(cl.getResource("res/right.gif")));
		right.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				currentRule++;
				eca.showRule(currentRule);
				rule.setText("Rule: " + currentRule);			
			}
		});
		
		// add items
		toolbar.add(left);
		toolbar.add(rule);
		toolbar.add(right);	
		
		frame.add(toolbar, BorderLayout.NORTH);
	}
			
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(ECAConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);			
			
		JMenuItem exitMenuItem = new JMenuItem(ECAConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);	
		
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(ECAConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(ECAConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
