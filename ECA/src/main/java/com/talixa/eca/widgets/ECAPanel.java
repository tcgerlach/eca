package com.talixa.eca.widgets;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ECAPanel extends JPanel {
	
	private static final int CELL_SIZE = 2;						// cell size in pixels
	private static final int BOARD_WIDTH = 250;					// board width in cells
	private static final int BOARD_HEIGHT = BOARD_WIDTH / 2;	// height is 1/2 of width
	
	private static boolean[][] cells;							// array of cells
	
	private static final Color EMPTY = Color.black;
	private static final Color LIFE = Color.green;
		
	public ECAPanel() {	
		// create board
		cells = new boolean[BOARD_WIDTH][BOARD_HEIGHT];
		
		// seed first row
		cells[BOARD_WIDTH/2][0] = true;
		
		// default to rule 30
		showRule(30);
	}
	
	public void showRule(int ruleNumber) {
		// easiest way to do this is to convert to a binary string
		String rule = Integer.toBinaryString(ruleNumber);
		while(rule.length() < 8) {
			rule = "0" + rule;
		}
		
		// create eca pattern
		for(int j = 1; j < BOARD_HEIGHT; ++j) {
			for(int i = 1; i < BOARD_WIDTH - 1; ++i) {
				boolean p1 = cells[i-1][j-1];
				boolean p2 = cells[i][j-1];
				boolean p3 = cells[i+1][j-1];
				
				// convert to integer
				int val = (p1 ? 0x04 : 0x00) | (p2 ? 0x02 : 0x00) | (p3 ? 0x01 : 0x00);
				
				// find rule - char index is left to right, bit index is right to left
				cells[i][j] = rule.charAt(7-val) == '1';
			}
		}
		invalidate();
		repaint();
	}
	
	@Override
	public void paint(Graphics g) {		
		super.paint(g);
		
		// fill space black
		g.setColor(EMPTY);
		g.fillRect(0, 0, CELL_SIZE * BOARD_WIDTH, CELL_SIZE * BOARD_HEIGHT);
		
		// fill live cells
		for(int i = 0; i < BOARD_WIDTH; ++i) {
			for(int j = 0; j < BOARD_HEIGHT; ++j) {
				if (cells[i][j]) {
					g.setColor(LIFE);
					g.fillRect(i*CELL_SIZE, j*CELL_SIZE, CELL_SIZE, CELL_SIZE);
				}
			}
		}
	}
}
